package SDA.pl.Zadanie01_choinka;


/*Zadanie 14. Napisać program rysujący w konsoli „choinkę” złożoną
        ze znaków gwiazdki (*). Użytkownik programu powinien podać liczbę
        całkowitą n, n > 0, określającą wysokość choinki (liczbę wierszy).
        Przykład: dla n = 5 wynik powinien wyglądać następująco:
            *
           ***
          *****
         *******
        *********
            *
        * */

import java.util.InputMismatchException;
import java.util.Scanner;

public class ChristmasTree {

    public static void main(String[] args) {

        try {

                    Scanner scanner = new Scanner(System.in);

            System.out.println("podaj wysokość choinki (1-30)");

            CTBuilder ctBuilder = new CTBuilder(scanner.nextByte());

            ctBuilder.builtTheTree();
        }catch (InputMismatchException e) {
            System.err.println("błędny parametr");
        }


    }


}
