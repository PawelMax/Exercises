package SDA.pl.Zadanie01_choinka;

public class CTBuilder {

    private byte numberOfLines;
    final char space = ' ';
    final char point = '*';

    public CTBuilder(byte numberOfLines) {
        this.numberOfLines = numberOfLines;
    }

    public void builtTheTree() {

        if (numberOfLines <= 30 && numberOfLines >= 1 ) {

            for (byte i = 1; i <= numberOfLines; i++) {
                byte spaceNumer = (byte) (numberOfLines - i);
                byte pointNumber = (byte) (1 + 2 * (i - 1));
                for (byte j = 1; j <= spaceNumer; j++) {
                    System.out.print(space);
                }
                for (byte k = 1; k <= pointNumber; k++) {
                    System.out.print(point);
                }
                System.out.println();
            }
            for (byte i = 1; i < numberOfLines; i++) {
                System.out.print(space);
            }
            System.out.print(point);

        } else {
            System.out.println("błędny parametr");
        }
    }
}
